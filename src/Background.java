import java.awt.*;

public class Background implements GameObject {

    private float timeUpdate = 5;
    private float backgroundTime = 0;

    private Color color;

    Background(float timeUpdate) {
        this.timeUpdate = timeUpdate;
    }

    @Override
    public void update(GameCanvas gameCanvas, float deltaTime) {
        backgroundTime += deltaTime;
        if (backgroundTime >= timeUpdate) {
            color = new Color((int)(Math.random() * 256f),
                              (int)(Math.random() * 256f),
                              (int)(Math.random() * 256f));
            backgroundTime = 0;
        }
    }

    @Override
    public void render(GameCanvas gameCanvas, Graphics g) {
        gameCanvas.setBackground(color);
    }
}
