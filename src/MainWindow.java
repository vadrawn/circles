
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

public class MainWindow extends JFrame{

    private static final int POS_X = 600;
    private static final int POS_Y = 200;
    private static final int WINDOW_WIDTH = 800;
    private static final int WINDOW_HEIGHT = 600;

    private int cntGameObject;
    GameObject[] gameObjects = new GameObject[1];

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainWindow();
            }
        });
    }

    MainWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(POS_X, POS_Y);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setResizable(false);
        setTitle("Circles");
        GameCanvas gameCanvas = new GameCanvas(this);

        // Добавление кружков по ЛКМ и удаление кружков по ПКМ
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                if (e.getButton() == MouseEvent.BUTTON1)
                    addGameObject(new Ball(e.getX(), e.getY()));
                else if (e.getButton() == MouseEvent.BUTTON3)
                    removeGameObject();
            }
        });

        add(gameCanvas);
        initGame();

        setVisible(true);
    }

    private void addGameObject(GameObject gameObject) {
        if (cntGameObject == gameObjects.length) {
            GameObject[] newGameObjects = Arrays.copyOf(gameObjects, gameObjects.length*2);
            gameObjects = newGameObjects;
        }
        gameObjects[cntGameObject++] = gameObject;
    }

    private void removeGameObject() {
        if (cntGameObject > 1)
            gameObjects[--cntGameObject] = null;
    }

    private void initGame() {
        addGameObject(new Background(2));
        addGameObject(new Ball());
    }

    void onDrawFrame(GameCanvas gameCanvas, Graphics g, float deltaTime) {
        update(gameCanvas, deltaTime);
        render(gameCanvas, g);
    }

    private void update(GameCanvas gameCanvas, float deltaTime) {
        for (int i = 0; i < cntGameObject; i++) {
            gameObjects[i].update(gameCanvas, deltaTime);
        }
    }

    private void render(GameCanvas gameCanvas, Graphics g) {
        for (int i = 0; i < cntGameObject; i++) {
            gameObjects[i].render(gameCanvas, g);
        }
    }
}
